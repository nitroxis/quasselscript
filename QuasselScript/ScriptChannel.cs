﻿using QuasselSharp;

namespace QuasselScript
{
	public sealed class ScriptChannel : ScriptObject
	{
		#region Fields

		private readonly IrcChannel channel;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ScriptChannel(ScriptSystem system, IrcChannel channel)
			: base(system)
		{
			this.channel = channel;

			this.BindProperty("encrypted", channel, "Encrypted");
			this.BindProperty("name", channel, "Name");
			this.BindProperty("password", channel, "Password");
			this.BindProperty("topic", channel, "Topic");
		}

		#endregion

		#region Methods

		public override bool Equals(object obj)
		{
			ScriptChannel other = obj as ScriptChannel;
			if (other == null)
				return false;

			return other.channel == this.channel;
		}

		public override int GetHashCode()
		{
			return this.channel.GetHashCode();
		}

		#endregion
	}
}
