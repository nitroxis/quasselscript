﻿using System.Collections.Generic;
using Jurassic;
using QuasselSharp;

namespace QuasselScript
{
	public sealed class ScriptCoreInfo : ScriptObject
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ScriptCoreInfo(ScriptSystem system)
			: base(system)
		{

		}

		#endregion

		#region Methods

		protected override object GetMissingPropertyValue(string propertyName)
		{
			CoreInfo info = this.System.SignalProxy.CoreInfo;
			if (info == null)
				return Null.Value;

			foreach (KeyValuePair<string, object> entry in info.CoreData)
			{
				if (entry.Key == propertyName)
					return ScriptSystem.ConvertToES(this.Engine, entry.Value);
			}

			return Null.Value;
		}

		#endregion
	}
}
