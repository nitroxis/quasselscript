﻿using Jurassic;
using QuasselSharp;

namespace QuasselScript
{
	public sealed class ScriptUser : ScriptObject
	{
		#region Fields

		private readonly IrcUser user;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ScriptUser.
		/// </summary>
		public ScriptUser(ScriptSystem system, IrcUser user)
			: base(system)
		{
			this.user = user;

			this.BindProperty("away", user, "Away");
			this.BindProperty("awayMessage", user, "AwayMessage");
			this.BindProperty("encrypted", user, "Encrypted");
			this.BindProperty("fullName", user, "FullName");
			this.BindProperty("host", user, "Host");
			this.BindProperty("idleTime", user, "IdleTime");
			this.BindProperty("ircOperator", user, "IrcOperator");
			this.BindProperty("lastAwayMessage", user, "LastAwayMessage");
			this.BindProperty("loginTime", user, "LoginTime");
			this.BindProperty("network", () => new ScriptNetwork(system, user.Network), null);
			this.BindProperty("nick", user, "Nick");
			this.BindProperty("realName", user, "RealName");
			this.BindProperty("server", user, "Server");
			this.BindProperty("suserHost", user, "SuserHost");
			this.BindProperty("user", user, "User");
			this.BindProperty("userModes", user, "UserModes");
			this.BindProperty("whoisServiceReply", user, "WhoisServiceReply");
		}

		#endregion

		#region Methods

		public override bool Equals(object obj)
		{
			ScriptUser other = obj as ScriptUser;
			if (other == null)
				return false;

			return other.user == this.user;
		}

		public override int GetHashCode()
		{
			return this.user.GetHashCode();
		}
		
		#endregion
	}
}
