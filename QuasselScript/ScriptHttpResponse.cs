﻿using Http;
using Jurassic;
using Jurassic.Library;

namespace QuasselScript
{
	public sealed class ScriptHttpResponse : ScriptObject
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ScriptHttpResponse(ScriptSystem system, HttpResponse response)
			: base(system)
		{
			this.BindProperty("reason", response, "Reason");
			this.BindProperty("status", response, "Status");

			this.BindProperty<object>("content", () =>
			{
				if (response.Content is StringContent)
					return ((StringContent)response.Content).Content;

				if (response.Content is FileContent)
					return new ScriptFile(this.System, ((FileContent)response.Content).File);

				return null;
			}, v =>
			{
				if (v == null || v == Null.Value || v == Undefined.Value)
				{
					response.Content = null;
				}
				if (v is ScriptFile)
				{
					response.Content = new FileContent(((ScriptFile)v).File);
				}
				else
				{
					response.Content = new StringContent(v.ToString());
				}
			});

			ObjectInstance headerObj = this.System.Engine.Object.Construct();
			foreach (HttpHeaderField field in response.Header.Fields)
				headerObj.SetPropertyValue(field.Name, field.Value, false);
			this.BindProperty("header", () => headerObj, null);
		}

		#endregion

		#region Methods

		#endregion
	}
}
