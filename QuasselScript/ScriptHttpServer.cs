﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using Http;
using Jurassic;
using Jurassic.Library;

namespace QuasselScript
{
	public sealed class ScriptHttpServer : ScriptObject
	{
		#region Fields

		private readonly HttpServer server;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ScriptHttpServer.
		/// </summary>
		public ScriptHttpServer(ScriptSystem system, HttpServer server)
			: base(system)
		{
			this.server = server;

			this.BindMethod("start", new Action(this.start));
			this.BindMethod("stop", new Action(this.stop));
			this.BindMethod("addHttpHost", new Action<string, int, string>(this.addHttpHost));
			this.BindMethod("addHandler", new Func<object, FunctionInstance, ScriptHttpRequestHandler>(this.addHandler));
		}

		#endregion

		#region Methods

		protected override void Dispose(bool managed)
		{
			if(managed)
				this.server.Stop();
		}

		private void start()
		{
			this.server.Start();
		}

		private void stop()
		{
			this.server.Stop();
		}

		private void addHttpHost(string ip, int port, string hostName = null)
		{
			IPAddress address = IPAddress.Parse(ip);
			HttpHost host = new HttpHost(address, port, hostName ?? ip);
			this.server.AddHost(host);
		}

		private ScriptHttpRequestHandler addHandler(object dest, FunctionInstance jsFunc)
		{
			RequestHandler handlerFunc = context =>
			{
				ScriptHttpContext ctx = new ScriptHttpContext(this.System, context);
				object value = jsFunc.CallLateBound(this, ctx);
				ctx.ApplyHeader();
				return !(value is bool && (bool)value == false);
			};
			
			if (dest is RegExpInstance)
			{
				Regex regex = new Regex(((RegExpInstance)dest).Source);
				RegexHandler handler = new RegexHandler(handlerFunc, regex);
				this.server.Handlers.Add(handler);
				return new ScriptHttpRequestHandler(this.System, handler);
			}

			if (dest is string)
			{
				UrlHandler handler = new UrlHandler(handlerFunc, (string)dest);
				this.server.Handlers.Add(handler);
				return new ScriptHttpRequestHandler(this.System, handler);
			}

			return null;
		}

		#endregion

	}
}
