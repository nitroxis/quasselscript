﻿using Jurassic;
using QuasselSharp;

namespace QuasselScript
{
	public class ScriptMessage : ScriptObject
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ScriptMessage.
		/// </summary>
		public ScriptMessage(ScriptSystem system, Message message)
			: base(system)
		{
			this.BindProperty("type", message, "Type");
			this.BindProperty("flags", message, "Flags");
			this.BindProperty("sender", message, "Sender");
			this.BindProperty("contents", message, "Contents");
			this.BindProperty("id", () => (int)message.MessageId, null);
			this.BindProperty("buffer", () => new ScriptBuffer(system, message.BufferInfo), null);
		}

		#endregion

		#region Methods

		#endregion
	}
}
