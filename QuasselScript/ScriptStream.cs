﻿using System;
using System.IO;
using System.Text;

namespace QuasselScript
{
	public sealed class ScriptStream : ScriptObject
	{
		#region Fields

		private readonly Stream stream;
		private StreamReader reader;
		private StreamWriter writer;
		
		private bool open;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ScriptStream(ScriptSystem system, Stream stream)
			: base(system)
		{
			this.stream = stream;
			
			this.open = true;

			this.BindMethod("readUnbuffered", new Func<int, string>(this.readUnbuffered));
			this.BindMethod("read", new Func<int, string>(this.read));
			this.BindMethod("readLine", new Func<string>(this.readLine));
			this.BindMethod("readToEnd", new Func<string>(this.readToEnd));

			this.BindMethod("write", new Func<string, bool>(this.write));
			this.BindMethod("writeLine", new Func<string, bool>(this.writeLine));

			this.BindMethod("close", new Action(this.close));
		}

		#endregion

		#region Methods

		private bool createReader()
		{
			if (this.reader != null)
				return true;

			if (stream.CanRead)
			{
				this.reader = new StreamReader(stream, new UTF8Encoding(false), true, 4096, true);
				return true;
			}

			return false;
		}

		private bool createWriter()
		{
			if (this.writer != null)
				return true;

			if (stream.CanWrite)
			{
				this.writer = new StreamWriter(stream, new UTF8Encoding(false), 4096, true);
				this.writer.AutoFlush = true;
				return true;
			}

			return false;
		}

		private string readUnbuffered(int numBytes)
		{
			byte[] buffer = new byte[numBytes];
			numBytes = this.stream.Read(buffer, 0, numBytes);
			return Encoding.Default.GetString(buffer, 0, numBytes);
		}

		private string read(int numChars = 1)
		{
			if (!this.createReader()) return null;

			char[] chars = new char[numChars];
			int charsRead = this.reader.Read(chars, 0, numChars);
			if (charsRead == 0)
				return null;

			return new string(chars, 0, charsRead);	
		}

		private string readLine()
		{
			if (!this.createReader()) return null;
			return this.reader.ReadLine();
		}

		private string readToEnd()
		{
			if (!this.createReader()) return null;
			return this.reader.ReadToEnd();
		}

		private bool write(object value)
		{
			if (!this.createWriter()) return false;
			this.writer.Write(value);
			return true;
		}

		private bool writeLine(object value)
		{
			if (!this.createWriter()) return false;
			this.writer.WriteLine(value);
			return true;
		}

		private void close()
		{
			if (!this.open)
				return;
			
			this.open = false;

			if (this.reader != null)
				this.reader.Close();
			
			if (this.writer != null)
				this.writer.Close();

			this.stream.Close();
		}

		#endregion
	}
}
