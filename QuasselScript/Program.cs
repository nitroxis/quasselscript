﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using QuasselSharp;
using QuasselSharp.Protocol;

namespace QuasselScript
{
	public static class Program
	{
		private static bool running;

		public static int Main(string[] args)
		{
			if (args.Length == 0)
			{
				Console.Error.WriteLine("Usage: QuasselScript user@host:port [scriptdirectory]");
				return 1;
			}

			string user;
			string password;
			string host;
			int port;

			Match match = Regex.Match(args[0], @"([^:]+)(:(.+))?@([^:]+)(:(\d+))?");
			if (!match.Success)
			{
				Console.Error.WriteLine("Invalid argument.");
				return 1;
			}	

			System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });

			user = match.Groups[1].Value;
			password = match.Groups[3].Success ? match.Groups[3].Value : Environment.GetEnvironmentVariable("QUASSELSCRIPTPW");
			host = match.Groups[4].Value;
			port = match.Groups[6].Success ? int.Parse(match.Groups[6].Value, CultureInfo.InvariantCulture) : 4242;

			
			Thread scriptThread = new Thread(scriptMain);
			scriptThread.Priority = ThreadPriority.BelowNormal;
			
			
			Client client = new Client();
			
			SignalProxy sigProxy = new SignalProxy(ProxyMode.Client);
			Console.WriteLine("Connecting to {0}:{1}...", host, port);
			RemotePeer peer = client.Connect(host, port);
			sigProxy.AddPeer(peer);


			var auth = new BotAuthHandler(peer, user, password);
			peer.AuthHandler = auth;
			auth.HandshakeComplete += (sender, e) =>
			{
				foreach (BufferInfo buffer in e.SessionState.BufferInfos)
					sigProxy.BufferSyncer.AddBuffer(buffer);

				foreach (Identity identity in e.SessionState.Identities)
					sigProxy.AddIdentity(identity);

				foreach (NetworkId networkId in e.SessionState.NetworkIds)
					peer.Dispatch(new InitRequest("Network", ((int)networkId).ToString(CultureInfo.InvariantCulture)));

				scriptThread.Start(sigProxy);
			};

			auth.Start();

			if (args.Length >= 2)
				Environment.CurrentDirectory = args[1];

			running = true;
			while (peer.ReadMessage()) ;
			running = false;

			client.Disconnect();
			if (!scriptThread.Join(5000))
				scriptThread.Abort();
			
			return 0;
		}

		private static void scriptMain(object parameters)
		{
			SignalProxy sigProxy = (SignalProxy)parameters;

			ScriptSystem script = new ScriptSystem(sigProxy);
			script.Start();

			DateTime nextCoreInfoUpdate = DateTime.Now;

			while (running)
			{
				Thread.Sleep(10);

				if (nextCoreInfoUpdate < DateTime.Now)
				{
					foreach (Peer peer in sigProxy.Peers)
						peer.Dispatch(new InitRequest("CoreInfo", ""));

					nextCoreInfoUpdate += new TimeSpan(0, 0, 5);
				}

				lock (sigProxy.ProcessLock)
					script.Tick();
			}

			script.Stop();
		}
	}
}
