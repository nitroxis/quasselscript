﻿using System;
using Jurassic;
using Jurassic.Library;

namespace QuasselScript
{
	public sealed class SetTimerFunction : FunctionInstance
	{
		#region Fields

		private readonly ScriptSystem system;
		private readonly bool once;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public SetTimerFunction(ScriptSystem host, bool once)
			: base(host.Engine)
		{
			this.system = host; 
			this.once = once;
		}

		#endregion

		#region Methods

		public override object CallLateBound(object thisObject, params object[] argumentValues)
		{
			if (argumentValues.Length < 2)
				return Null.Value;

			FunctionInstance func = argumentValues[0] as FunctionInstance;

			if (func == null)
				return Null.Value;

			if (!(argumentValues[1] is int))
				return Null.Value;

			TimeSpan interval = new TimeSpan(0, 0, 0, 0, (int)argumentValues[1]);

			object[] parameters = new object[argumentValues.Length - 2];
			Array.Copy(argumentValues, 2, parameters, 0, parameters.Length);

			ScriptTimer timer = new ScriptTimer(this.system, func, thisObject, interval, this.once, parameters);
			Console.WriteLine("Added timer {0} with interval {1}.", func.DisplayName, interval);

			return timer;
		}

		#endregion
	}
}
