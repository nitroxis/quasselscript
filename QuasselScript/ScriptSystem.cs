﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Jurassic;
using Jurassic.Library;
using QuasselSharp;

namespace QuasselScript
{
	public sealed class ScriptSystem
	{
		private class ScriptFile : ScriptSource
		{
			public string Name;
			public FileInfo File;

			public override TextReader GetReader()
			{
				return this.File.OpenText();
			}

			public override string Path
			{
				get { return this.Name; }
			}
		}

		#region Fields

		private readonly SignalProxy signalProxy;
	
		private ScriptEngine engine;
		private ArrayInstance errorArray;
		private List<WeakReference<ScriptObject>> objectReferences;
		private List<ScriptTimer> timers;
		private List<ScriptFile> scriptFiles;

		private DateTime nextGC;

		#endregion

		#region Properties

		public SignalProxy SignalProxy
		{
			get { return this.signalProxy; }
		}

		public ScriptEngine Engine
		{
			get { return this.engine; }
		}

		/*public IEnumerable<ScriptObject> Objects
		{
			get
			{
				for (int i = this.objectReferences.Count - 1; i >= 0; i--)
				{
					ScriptObject obj;
					if (!this.objectReferences[i].TryGetTarget(out obj))
					{
						this.objectReferences.RemoveAt(i);
						continue;
					}

					yield return obj;
				}
			}
		}*/

		#endregion

		#region Constructors

		public ScriptSystem(SignalProxy signalProxy)
		{
			this.signalProxy = signalProxy;
		}

		#endregion

		#region Methods

		private void run(string name, bool once = false)
		{
			if (name != null)
			{
				FileInfo file = new FileInfo(name);
				
				if (once)
				{
					foreach (ScriptFile f in this.scriptFiles)
					{
						if (f.File.FullName == file.FullName)
							return;
					}
				}

				if (!file.Exists)
				{
					Console.Error.WriteLine("Could not find script \"{0}\".", name);
					return;
				} 
				
				ScriptFile scriptFile = new ScriptFile { Name = name, File = file };
				this.scriptFiles.Add(scriptFile);

				try
				{
					Console.WriteLine("Executing \"{0}\"...", name);
					this.engine.Execute(scriptFile);
				}
				catch (Exception ex)
				{
					this.AddError(ex);

					Console.Error.WriteLine("Error in \"{0}\": {1}", file.Name, ex);
				}
			}
			else
			{
				FileInfo[] files = new DirectoryInfo(Environment.CurrentDirectory).GetFiles("*.js");
				foreach (FileInfo f in files)
					this.run(f.Name, true);
			}
		}

		public void AddError(Exception ex)
		{
			JavaScriptException jsex = ex as JavaScriptException;
			if (jsex != null)
			{
				string errorStr = string.Format("{0}:{1} {2} {3}", jsex.SourcePath, jsex.LineNumber, jsex.Message, jsex.FunctionName != null ? " (" + jsex.FunctionName + ")" : "");
				Console.Error.WriteLine(errorStr);
				this.errorArray.Push(errorStr);
			}
			else
			{
				Console.Error.WriteLine(ex);
				this.errorArray.Push(ex.ToString());
			}
		}

		public void AddObject(ScriptObject obj)
		{
			lock(this.objectReferences)
				this.objectReferences.Add(new WeakReference<ScriptObject>(obj));

			if (obj is ScriptTimer)
			{
				lock(this.timers)
					this.timers.Add((ScriptTimer)obj);
			}				
		}

		public void Start()
		{
			this.objectReferences = new List<WeakReference<ScriptObject>>();
			this.timers = new List<ScriptTimer>();

			this.engine = new ScriptEngine();
			this.errorArray = this.engine.Array.New();
			this.engine.SetGlobalValue("stdout", new ScriptStream(this, Console.OpenStandardOutput()));
			this.engine.SetGlobalValue("stderr", new ScriptStream(this, Console.OpenStandardError()));
			this.engine.SetGlobalValue("errors", this.errorArray);
			this.engine.SetGlobalValue("run", new LateBoundFunction(this.engine, new Action<string, bool>(this.run)));
			this.engine.SetGlobalValue("fileSystem", new ScriptFileSystem(this));
			var net = new ScriptNet(this);
            this.engine.SetGlobalValue("net", net);
			this.engine.SetGlobalValue("web", net);
			this.engine.SetGlobalValue("setTimeout", new SetTimerFunction(this, true));
			this.engine.SetGlobalValue("setInterval", new SetTimerFunction(this, false));
			this.engine.SetGlobalValue("clearTimeout", new LateBoundFunction(this.engine, new Action<ScriptTimer>(this.removeTimer)));
			this.engine.SetGlobalValue("clearInterval", new LateBoundFunction(this.engine, new Action<ScriptTimer>(this.removeTimer)));
			this.engine.SetGlobalValue("restart", new LateBoundFunction(this.engine, new Action(this.Restart)));
			this.engine.SetGlobalValue("quassel", new ScriptQuassel(this));
			
			this.scriptFiles = new List<ScriptFile>();
			this.run("init.js");

			this.nextGC = DateTime.Now;
		}

		public void Stop()
		{
			lock(this.objectReferences)
			{
				foreach (WeakReference<ScriptObject> reference in this.objectReferences)
				{
					ScriptObject obj;
					if (!reference.TryGetTarget(out obj))
						continue;

					obj.Dispose();
				}

				this.objectReferences.Clear();
			}

			this.errorArray = null;
			this.engine = null;
		}

		public void Restart()
		{
			this.Stop();
			this.Start();
		}

		private void collectGarbage()
		{
			lock (this.objectReferences)
			{
				this.objectReferences.RemoveAll(r =>
				{
					ScriptObject obj;
					return r.TryGetTarget(out obj);
				});
			}
		}

		public void Tick()
		{
			if(DateTime.Now > this.nextGC)
			{
				this.collectGarbage();
				this.nextGC = DateTime.Now + new TimeSpan(0, 0, 5);
			}

			ScriptTimer[] timersCopy;
			lock(this.timers)
				timersCopy = this.timers.ToArray();

			foreach (ScriptTimer timer in timersCopy)
            {
				try
				{
					if(timer.Tick() && timer.Once)
						this.removeTimer(timer);
				}
				catch (Exception ex)
				{
					this.AddError(ex);
					Console.Error.WriteLine(timer.Function);
				}
			}
		}

		private void removeTimer(ScriptTimer timer)
		{
			Console.WriteLine("Timer {0} removed.", timer.Function.DisplayName);

			lock(this.timers)
				this.timers.Remove(timer);
			
			lock (this.objectReferences)
			{
				this.objectReferences.RemoveAll(r =>
				{
					ScriptObject obj;
					if (!r.TryGetTarget(out obj))
						return true;

					return obj == timer;
				});
			}			
		}

		internal static object ConvertToES(ScriptEngine engine, object value)
		{
			if (value == null)
				return Null.Value;

			if (IsESNative(value))
				return value;

			if (value.GetType().IsArray)
			{
				IList source = (IList)value;
				object[] array = new object[source.Count];
				for (int i = 0; i < array.Length; i++)
				{
					if (source[i] == null)
						array[i] = Null.Value;
					else if (ScriptSystem.IsESNative(source[i]))
						array[i] = source[i];
					else
						throw new NotSupportedException(string.Format("\"{0}\" is not supported in ECMA script.", source[i].GetType().Name));
				}
				return engine.Array.New(array);
			}

			if (value.GetType().IsEnum)
			{
				value = Convert.ChangeType(Convert.ChangeType(value, value.GetType().GetEnumUnderlyingType()), typeof(double));
				return engine.Number.Construct((double)value);
			}

			throw new NotSupportedException(string.Format("\"{0}\" is not supported in ECMA script.", value.GetType().Name));
		}

		internal static object[] ConvertToES(ScriptEngine engine, object[] values)
		{
			if (values == null)
				return null;
			object[] wrapped = new object[values.Length];
			for (int i = 0; i < values.Length; i++)
				wrapped[i] = ConvertToES(engine, values[i]);
			return wrapped;
		}

		internal static object ConvertToNet(object value)
		{
			if (value is ArrayInstance)
				return new List<object>(((ArrayInstance)value).ElementValues).ToArray(); // TODO: make this prettier

			if (value is ClrInstanceWrapper)
				return ((ClrInstanceWrapper)value).WrappedInstance;

			if (value is ConcatenatedString)
				return value.ToString();

			if (value is NumberInstance)
				return ((NumberInstance)value).Value;

			return value;
		}

		internal static bool IsESNative(object value)
		{
			return value is ObjectInstance || value is bool || value is int || value is uint || value is double || value is string || value is ConcatenatedString || value is Null || value is Undefined;
		}


		#endregion
	}
}
