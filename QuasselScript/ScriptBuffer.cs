﻿using System;
using Jurassic;
using QuasselSharp;

namespace QuasselScript
{
	public sealed class ScriptBuffer : ScriptObject
	{
		#region Fields

		private readonly BufferInfo buffer;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ScriptBuffer(ScriptSystem system, BufferInfo buffer)
			: base(system)
		{
			this.buffer = buffer;

			this.BindProperty("id", () => (int)buffer.Id, null);
			this.BindProperty("name", buffer, "Name");
			this.BindProperty("networkID", () => (int)buffer.NetworkId, null);
			this.BindProperty("type", buffer, "Type");
			this.BindMethod("send", new Action<object>(o => system.SignalProxy.SendMessage(buffer, o.ToString())));
		}

		#endregion

		#region Methods

		public override bool Equals(object obj)
		{
			ScriptBuffer other = obj as ScriptBuffer;
			if (other == null)
				return false;

			return other.buffer.Id == this.buffer.Id;
		}

		public override int GetHashCode()
		{
			return (int)this.buffer.Id;
		}

		public override string ToString()
		{
			return string.Format("Buffer {0}", this.buffer.Id);
		}

		#endregion
	}
}
