﻿using System.Collections.Generic;
using Http;
using Jurassic;
using Jurassic.Library;

namespace QuasselScript
{
	public sealed class ScriptHttpRequest : ScriptObject
	{
		#region Fields

		private readonly HttpRequest request;

		#endregion

		#region Properties

		private string method
		{
			get
			{

				switch (request.Method)
				{
					case HttpMethod.Get:
						return "GET";

					case HttpMethod.Post:
						return "POST";

					case HttpMethod.Head:
						return "HEAD";
				}

				return null;
			}
		}

		#endregion

		#region Constructors

		public ScriptHttpRequest(ScriptSystem system, HttpRequest request)
			: base(system)
		{
			this.request = request;

			ObjectInstance queryObj = this.System.Engine.Object.Construct();
			foreach (KeyValuePair<string, string> q in this.request.Query)
				queryObj.SetPropertyValue(q.Key, (object)q.Value ?? Null.Value, false);
			this.BindProperty("query", () => queryObj, null);

			ObjectInstance headerObj = this.System.Engine.Object.Construct();
			foreach (HttpHeaderField field in this.request.Header.Fields)
				headerObj.SetPropertyValue(field.Name, field.Value, false);
			this.BindProperty("header", () => headerObj, null);
	
			this.BindProperty("method", this, "method");
			this.BindProperty("url", request, "URL");
		}

		#endregion

		#region Methods
		
		#endregion
	}
}
