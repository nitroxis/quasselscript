﻿using System;
using System.IO;
using System.Net;
using Http;
using Jurassic.Library;

namespace QuasselScript
{
	public sealed class ScriptNet : ScriptObject
	{
        #region Constructors

        /// <summary>
        /// Creates a new ScriptNet.
        /// </summary>
        public ScriptNet(ScriptSystem system)
			: base(system)
		{
			this.BindMethod("download", new Func<string, string>(this.download));
			this.BindMethod("upload", new Func<string, string, string>(this.upload));
			this.BindMethod("openRead", new Func<string, ScriptStream>(this.openRead));
			this.BindMethod("createServer", new Func<ScriptHttpServer>(this.createServer));
        }

		#endregion

		#region Methods

		private string download(string url)
		{
			try
			{
				using(WebClient client = new WebClient())
					return client.DownloadString(url);
			}
			catch(Exception e)
			{
				Console.Error.WriteLine(e.ToString());
				return null;
			}
		}

		private string upload(string url, string data)
		{
			try
			{
				using (WebClient client = new WebClient())
					return client.UploadString(url, data);
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e.ToString());
				return null;
			}
		}

		private ScriptStream openRead(string url)
		{
			try
			{
				using (WebClient client = new WebClient())
				{
					Stream stream = client.OpenRead(url);
					if (stream == null) return null;
					return new ScriptStream(this.System, stream);
				}
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e.ToString());
				return null;
			}
		}
        
		private ScriptHttpServer createServer()
		{
			HttpServer httpServer = new HttpServer();
			httpServer.ApplicationName = "QuasselScript";
			return new ScriptHttpServer(this.System, httpServer);
		}

        #endregion
    }
}
