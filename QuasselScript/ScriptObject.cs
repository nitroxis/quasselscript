﻿using System;
using System.Reflection;
using Jurassic.Library;

namespace QuasselScript
{
	public abstract class ScriptObject : ObjectInstance, IDisposable
	{
		#region Fields

		private readonly ScriptSystem system;
		private bool isDisposed;

		#endregion

		#region Properties

		public ScriptSystem System
		{
			get { return this.system; }
		}


		public bool IsDisposed
		{
			get { return this.isDisposed; }
		}
		
		#endregion

		#region Constructors

		protected ScriptObject(ScriptSystem system)
			: base(system.Engine)
		{
			this.system = system;
			this.system.AddObject(this);
		}

		#endregion

		#region Methods

		#region IDisposable implementation
		
		/// <summary>
		/// Disposes the object and releases all resources.
		/// </summary>
		public void Dispose()
		{
			if (!this.isDisposed)
				this.Dispose(true);
			this.isDisposed = true;
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool managed)
		{

		}

		~ScriptObject()
		{
			if (!this.isDisposed)
				this.Dispose(false);
			this.isDisposed = true;
		}

		#endregion

		protected void BindProperty(string jsName, object obj, string objProperty)
		{
			if (obj == null)
				throw new ArgumentNullException("obj");

			Type type = obj.GetType();

			PropertyInfo property = type.GetProperty(objProperty, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			if (property == null)
				throw new Exception("Property does not exist.");

			Type getDelegate = typeof(Func<>).MakeGenericType(property.PropertyType);
			Type setDelegate = typeof(Action<>).MakeGenericType(property.PropertyType);

			PropertyDescriptor descriptor = new PropertyDescriptor(
				property.GetMethod == null ? null : new LateBoundFunction(this.Engine, property.GetMethod.CreateDelegate(getDelegate, obj)),
				property.SetMethod == null ? null : new LateBoundFunction(this.Engine, property.SetMethod.CreateDelegate(setDelegate, obj)), Jurassic.Library.PropertyAttributes.Sealed);
			
			this.DefineProperty(jsName, descriptor, true);
		}

		protected void BindProperty<T>(string jsName, Func<T> getMethod, Action<T> setMethod)
		{
			PropertyDescriptor descriptor = new PropertyDescriptor(
				getMethod == null ? null : new LateBoundFunction(this.Engine, getMethod),
				setMethod == null ? null : new LateBoundFunction(this.Engine, setMethod), Jurassic.Library.PropertyAttributes.Sealed);

			this.DefineProperty(jsName, descriptor, true);
		}

		protected void BindMethod(string jsName, Delegate func)
		{
			LateBoundFunction funcWrapper = new LateBoundFunction(this.Engine, func);
			PropertyDescriptor descriptor = new PropertyDescriptor(funcWrapper, Jurassic.Library.PropertyAttributes.Sealed);
			this.DefineProperty(jsName, descriptor, true);
		}

		#endregion
	}
}
