﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Jurassic;
using Jurassic.Library;

namespace QuasselScript
{
	public sealed class ScriptFileSystem : ScriptObject
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ScriptFileSystem(ScriptSystem system)
			: base(system)
		{
			this.BindMethod("get", new Func<string, ScriptFile>(this.get));
			this.BindMethod("search", new Func<string, RegExpInstance, ArrayInstance>(this.search));
			this.BindMethod("delete", new Func<string, bool>(this.delete));
		}

		#endregion

		#region Methods

		private ScriptFile get(string name)
		{
			if (name == null)
				return null;

			FileInfo file = new FileInfo(name);
			return new ScriptFile(this.System, file);
		}

		private ArrayInstance search(string path, RegExpInstance regex)
		{
			DirectoryInfo currentDir = new DirectoryInfo(path);
			FileInfo[] files = currentDir.GetFiles("*", SearchOption.AllDirectories);
			ArrayInstance array = this.System.Engine.Array.New();

			foreach (FileInfo file in files)
			{
				if (regex.Test(file.Name))
					ArrayInstance.Push(array, new ScriptFile(this.System, file));
			}

			return array;
		}

		private bool delete(string name)
		{
			if (name == null)
				return false;

			File.Delete(name);
			return true;
		}
	
		#endregion
	}
}
