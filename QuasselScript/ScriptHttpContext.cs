﻿using Http;
using Jurassic.Library;

namespace QuasselScript
{
	public sealed class ScriptHttpContext : ScriptObject
	{
		#region Fields

		private readonly HttpContext context;
		private ScriptStream stream;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ScriptHttpContext.
		/// </summary>
		public ScriptHttpContext(ScriptSystem system, HttpContext context)
			: base(system)
		{
			this.context = context;

			this.BindProperty("server", () => new ScriptHttpServer(this.System, context.Server), null);
			this.BindProperty("request", () => new ScriptHttpRequest(this.System, context.Request), null);
			this.BindProperty("response", () => new ScriptHttpResponse(this.System, context.Response), null);
			this.BindProperty("stream", () => this.stream ?? (this.stream = new ScriptStream(this.System, context.Stream)), null);
		}

		#endregion

		#region Methods

		public void ApplyHeader()
		{
			ScriptHttpResponse value = this.GetPropertyValue("response") as ScriptHttpResponse;
			if (value == null)
				return;

			ObjectInstance header = value.GetPropertyValue("header") as ObjectInstance;
			if (header == null)
				return;

			this.context.Response.Header.Fields.Clear();
			foreach (PropertyNameAndValue property in header.Properties)
			{
				this.context.Response.Header.Fields.Add(new HttpHeaderField(property.Name, property.Value.ToString()));
			}
		}
		
		#endregion
	}
}
