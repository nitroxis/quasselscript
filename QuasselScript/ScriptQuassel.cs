﻿using System;
using System.Collections.Generic;
using Jurassic;
using Jurassic.Library;
using QuasselSharp;

namespace QuasselScript
{
	public sealed class ScriptQuassel : ScriptObject
	{
		#region Fields

		private readonly Dictionary<string, List<FunctionInstance>> events;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ScriptQuassel.
		/// </summary>
		public ScriptQuassel(ScriptSystem system)
			: base(system)
		{
			this.events = new Dictionary<string, List<FunctionInstance>>();

			this.System.SignalProxy.Message += this.onMessage;

			this.addEvent("tick");
			this.addEvent("message");

			this.addEvent("plain");
			this.addEvent("notice");
			this.addEvent("action");
			this.addEvent("nick");
			this.addEvent("mode");
			this.addEvent("join");
			this.addEvent("part");
			this.addEvent("quit");
			this.addEvent("kick");
			this.addEvent("kill");
			this.addEvent("server");
			this.addEvent("info");
			this.addEvent("error");
			this.addEvent("topic");
			this.addEvent("invite");

			this.BindMethod("getNetwork", new Func<NetworkId, ScriptNetwork>(this.getNetwork));
			this.BindMethod("findNetwork", new Func<string, ScriptNetwork>(this.findNetwork));

			this.BindMethod("getBuffer", new Func<BufferId, ScriptBuffer>(this.getBuffer));
			this.BindMethod("findBuffer", new Func<string, ScriptBuffer>(this.findBuffer));

			this.BindProperty("coreInfo", () => this.System.SignalProxy.CoreInfo == null ? (object)Null.Value : new ScriptCoreInfo(this.System), null);
		}

		#endregion

		#region Methods

		protected override void Dispose(bool managed)
		{
			if(managed)
				this.System.SignalProxy.Message -= this.onMessage;
		}

		private void onMessage(object sender, MessageEventArgs e)
		{
			this.raiseEvent("message", new ScriptMessage(this.System, e.Message));

			ScriptBuffer buffer = new ScriptBuffer(this.System, e.Message.BufferInfo);

			switch (e.Message.Type)
			{
				case MessageType.Action:
					this.raiseEvent("action", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Error:
					this.raiseEvent("error", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Info:
					this.raiseEvent("info", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Invite:
					this.raiseEvent("invite", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Join:
					this.raiseEvent("join", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Kick:
					this.raiseEvent("kick", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Kill:
					this.raiseEvent("kill", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Mode:
					this.raiseEvent("mode", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Nick:
					this.raiseEvent("nick", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Notice:
					this.raiseEvent("notice", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Part:
					this.raiseEvent("part", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Plain:
					this.raiseEvent("plain", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Quit:
					this.raiseEvent("quit", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Server:
					this.raiseEvent("server", buffer, e.Message.Sender, e.Message.Contents);
					break;

				case MessageType.Topic:
					this.raiseEvent("topic", buffer, e.Message.Sender, e.Message.Contents);
					break;
			}
		}

		private void addEvent(string name)
		{
			this.events.Add(name, new List<FunctionInstance>());
			this.BindMethod(name, new Action<FunctionInstance>(f => this.events[name].Add(f)));
		}

		private void raiseEvent(string name, params object[] parameters)
		{
			foreach (FunctionInstance func in this.events[name])
			{
				try
				{
					func.CallLateBound(this, parameters);
				}
				catch (Exception ex)
				{
					this.System.AddError(ex);
					Console.Error.WriteLine(func);
				}
			}
		}

		private ScriptNetwork getNetwork(NetworkId id)
		{
			foreach (Network network in this.System.SignalProxy.Networks)
			{
				if (network.Id == id)
					return new ScriptNetwork(this.System, network);
			}

			return null;
		}

		private ScriptNetwork findNetwork(string name)
		{
			foreach (Network network in this.System.SignalProxy.Networks)
			{
				if (network.NetworkName == name)
					return new ScriptNetwork(this.System, network);
			}

			return null;
		}

		private ScriptBuffer getBuffer(BufferId id)
		{
			foreach (BufferInfo buffer in this.System.SignalProxy.BufferSyncer)
			{
				if (buffer.Id == id)
					return new ScriptBuffer(this.System, buffer);
			}

			return null;
		}
		
		private ScriptBuffer findBuffer(string name)
		{
			foreach (BufferInfo buffer in this.System.SignalProxy.BufferSyncer)
			{
				if (buffer.Name == name)
					return new ScriptBuffer(this.System, buffer);
			}

			return null;
		}

		#endregion
	}
}

