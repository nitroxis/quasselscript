﻿using System;
using System.Collections;
using System.Reflection;
using Jurassic;
using Jurassic.Library;

namespace QuasselScript
{
	public class LateBoundFunction : FunctionInstance
	{
		#region Fields

		private readonly Delegate func;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LateBoundFunction.
		/// </summary>
		public LateBoundFunction(ScriptEngine engine, Delegate func)
			: base(engine)
		{
			this.func = func;
		}

		#endregion

		#region Methods

		public override object CallLateBound(object thisObject, params object[] argumentValues)
		{
			ParameterInfo[] parameters = this.func.Method.GetParameters();
			object[] values = new object[parameters.Length];

			for (int i = 0; i < parameters.Length; i++)
			{
				if (i >= argumentValues.Length || argumentValues[i] == null)
				{
					if (parameters[i].HasDefaultValue)
						values[i] = parameters[i].DefaultValue;
					else if (parameters[i].ParameterType.IsValueType)
						values[i] = Activator.CreateInstance(parameters[i].ParameterType);
					else
						values[i] = null;
				}
				else
				{
					values[i] = ScriptSystem.ConvertToNet(argumentValues[i]);

					if (!parameters[i].ParameterType.IsInstanceOfType(values[i]))
					{
						if (parameters[i].ParameterType.IsEnum)
							values[i] = Enum.ToObject(parameters[i].ParameterType, Convert.ChangeType(values[i], parameters[i].ParameterType.GetEnumUnderlyingType()));
						else
							values[i] = Convert.ChangeType(values[i], parameters[i].ParameterType);
					}
				}
			}

			object result = this.func.DynamicInvoke(values);

			return ScriptSystem.ConvertToES(this.Engine, result);
		}

		public override string ToString()
		{
			return this.func.Method.Name;
		}

		#endregion

	}

}
