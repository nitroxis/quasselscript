﻿using System;
using Jurassic;
using QuasselSharp;

namespace QuasselScript
{
	public sealed class ScriptNetwork : ScriptObject
	{
		#region Fields

		private readonly Network network;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public ScriptNetwork(ScriptSystem system, Network network)
			: base(system)
		{
			this.network = network;
			
			this.BindProperty("id", () => (int)network.Id, null);

			this.BindProperty("autoIdentifyPassword", network, "AutoIdentifyPassword");
			this.BindProperty("autoIdentifyService", network, "AutoIdentifyService");
			this.BindProperty("autoReconnectInterval", network, "AutoReconnectInterval");
			this.BindProperty("autoReconnectRetries", network, "AutoReconnectRetries");
			this.BindProperty("connectionState", network, "ConnectionState");
			this.BindProperty("currentServer", network, "CurrentServer");
			this.BindProperty("identityId", network, "IdentityId");
			this.BindProperty("isConnected", network, "IsConnected");
			this.BindProperty("latency", network, "Latency");
			this.BindProperty("myNick", network, "MyNick");
			this.BindProperty("name", network, "NetworkName");
			this.BindProperty("perform", network, "Perform");
			this.BindProperty("rejoinChannels", network, "RejoinChannels");
			this.BindProperty("saslAccount", network, "SaslAccount");
			this.BindProperty("saslPassword", network, "SaslPassword");
			this.BindProperty("unlimitedReconnectRetries", network, "UnlimitedReconnectRetries");
			this.BindProperty("useAutoIdentify", network, "UseAutoIdentify");
			this.BindProperty("useAutoReconnect", network, "UseAutoReconnect");
			this.BindProperty("useRandomServer", network, "UseRandomServer");
			this.BindProperty("useSasl", network, "UseSasl");

			this.BindProperty("statusBuffer", this, "statusBuffer");

			this.BindMethod("findUser", new Func<string, ScriptUser>(this.findUser));
			this.BindMethod("findUserByNick", new Func<string, ScriptUser>(this.findUserByNick));
			this.BindMethod("findChannel", new Func<string, ScriptChannel>(this.findChannel));
			this.BindMethod("findBuffer", new Func<string, ScriptBuffer>(this.findBuffer));
		}

		#endregion

		#region Methods

		private ScriptBuffer statusBuffer
		{
			get
			{
				foreach (BufferInfo buffer in this.System.SignalProxy.BufferSyncer)
				{
					if (buffer.Type == BufferType.StatusBuffer && buffer.NetworkId == this.network.Id)
						return new ScriptBuffer(this.System, buffer);
				}

				return null;
			}
		}

		private ScriptUser findUser(string fullName)
		{
			IrcUser user = this.network.GetUser(fullName);
			if (user == null)
				return null;

			return new ScriptUser(this.System, user);
		}
		
		private ScriptUser findUserByNick(string nick)
		{
			IrcUser user = this.network.GetUserByNick(nick);
			if (user == null)
				return null;

			return new ScriptUser(this.System, user);
		}

		private ScriptChannel findChannel(string name)
		{
			IrcChannel channel = this.network.GetChannel(name);
			if (channel == null)
				return null;

			return new ScriptChannel(this.System, channel);
		}

		private ScriptBuffer findBuffer(string name)
		{
			foreach (BufferInfo buffer in this.System.SignalProxy.BufferSyncer)
			{
				if (buffer.NetworkId == this.network.Id && buffer.Name == name)
					return new ScriptBuffer(this.System, buffer);
			}

			return null;
		}

		public override bool Equals(object obj)
		{
			ScriptNetwork other = obj as ScriptNetwork;
			if (other == null)
				return false;

			return other.network.Id == this.network.Id;
		}

		public override int GetHashCode()
		{
			return (int)this.network.Id;
		}

		public override string ToString()
		{
			return string.Format("Network {0}", this.network.Id);
		}

		#endregion
	}
}
