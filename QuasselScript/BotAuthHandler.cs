﻿using System;
using System.Text;
using QuasselSharp;
using QuasselSharp.Protocol;

namespace QuasselScript
{
	/// <summary>
	/// Automatic/console auth handler.
	/// </summary>
	public class BotAuthHandler : AuthHandler
	{
		#region Fields

		private readonly Peer peer;

		private string user;
		private string password;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BotAuthHandler.
		/// </summary>
		public BotAuthHandler(Peer peer, string user, string password)
		{
			this.peer = peer;

			this.user = user;
			this.password = password;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Starts authentication.
		/// </summary>
		public void Start()
		{
			Console.WriteLine("Registering client...");
			this.peer.Dispatch(new RegisterClient("QuasselScript", ""));
		}

		public override void Handle(HandshakeMessage message)
		{
			if (message is ClientRegistered)
			{
				Console.WriteLine("Client registered.");
				this.login();
			}
			else if (message is LoginFailed)
			{
				Console.WriteLine("Login failed, please try again.");
				this.password = null;
				this.login();
			}
			else if (message is LoginSuccess)
			{
				Console.WriteLine("Login successful.");
				this.password = null;
			}
			else if (message is SessionState)
			{
				Console.WriteLine("Received session state.");
				this.OnHandshakeComplete(new HandshakeCompleteEventArgs((SessionState)message));
			}
			else
			{
				Console.WriteLine("Unhandled message: {0}", message);
			}
		}

		private void login()
		{
			string loginUser;
			string loginPassword;

			if (this.user == null)
			{
				Console.Write("User: ");
				loginUser = Console.ReadLine();
			}
			else
			{
				loginUser = this.user;
			}

			if (this.password == null)
			{
				Console.Write("Password for {0}: ", loginUser);
				loginPassword = readPassword();
			}
			else
			{
				loginPassword = this.password;
			}
			
			this.peer.Dispatch(new Login(loginUser, loginPassword));
		}

		private static string readPassword()
		{
			StringBuilder password = new StringBuilder();

			while (true)
			{
				ConsoleKeyInfo key = Console.ReadKey(true);
				if (key.Key == ConsoleKey.Backspace)
				{
					if (password.Length > 0)
						password.Remove(password.Length - 1, 1);
				}
				else if (key.Key == ConsoleKey.Enter)
				{
					Console.WriteLine();
					return password.ToString();
				}
				else
				{
					password.Append(key.KeyChar);
				}
			}
		}

		#endregion
	}
}
