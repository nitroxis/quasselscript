﻿using Http;

namespace QuasselScript
{
	public sealed class ScriptHttpRequestHandler : ScriptObject
	{
		#region Fields

		private readonly IRequestHandler handler;

		#endregion

		#region Properties

		public IRequestHandler Handler
		{
			get { return this.handler; }
		}

		#endregion

		#region Constructors

		public ScriptHttpRequestHandler(ScriptSystem system, IRequestHandler handler)
			: base(system)
		{
			this.handler = handler;
		}

		#endregion

		#region Methods

		#endregion
	}
}
