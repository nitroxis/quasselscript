﻿using System;
using Jurassic.Library;

namespace QuasselScript
{
	public class ScriptTimer : ScriptObject
	{
		#region Fields

		private DateTime lastInvoke;
		private TimeSpan interval;
		private bool once;

		private FunctionInstance function;
		private object[] parameters;

		#endregion

		#region Properties

		public bool Once
		{
			get { return this.once; }
		}

		public FunctionInstance Function
		{
			get { return this.function; }
		}

		#endregion

		#region Constructors

		public ScriptTimer(ScriptSystem system, FunctionInstance function, object thisObject, TimeSpan interval, bool once, object[] parameters)
			: base(system)
		{
			this.lastInvoke = DateTime.Now;
			this.interval = interval;
			this.once = once;

			this.function = function;
			this.parameters = parameters;
		}

		#endregion

		#region Methods

		public bool Tick()
		{
			DateTime now = DateTime.Now;

			bool funcCalled = false;

			while (this.lastInvoke + this.interval < now)
			{
				this.lastInvoke += this.interval;
				this.function.CallLateBound(this, this.parameters);
				funcCalled = true;
				if (this.once)
					break;
			}

			return funcCalled;
		}

		#endregion
	}
}
