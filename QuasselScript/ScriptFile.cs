﻿using System;
using System.IO;
using Jurassic;

namespace QuasselScript
{
	public sealed class ScriptFile : ScriptObject
	{
		#region Fields

		private readonly FileInfo file;

		#endregion

		#region Properties

		public FileInfo File
		{
			get { return this.file; }
		}

		#endregion

		#region Constructors

		public ScriptFile(ScriptSystem system, FileInfo file)
			: base(system)
		{
			this.file = file;

			this.BindProperty("name", file, "Name");
			this.BindProperty("fullName", file, "FullName");
			this.BindProperty("exists", file, "Exists");
			
			this.BindMethod("openRead", new Func<ScriptStream>(this.openRead));
			this.BindMethod("openWrite", new Func<ScriptStream>(this.openWrite));
			this.BindMethod("openAppend", new Func<ScriptStream>(this.openAppend));
		}

		#endregion

		#region Methods

		private ScriptStream openRead()
		{
			try
			{
				Stream stream = this.file.OpenRead();
				return new ScriptStream(this.System, stream);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine("Could not open file \"{0}\" for read access: {1}", this.file.Name, ex.Message);
				return null;
			}
		}

		private ScriptStream openWrite()
		{
			try
			{
				Stream stream = this.file.Create();
				return new ScriptStream(this.System, stream);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine("Could not open file \"{0}\" for write access: {1}", this.file.Name, ex.Message);
				return null;
			}
		}

		private ScriptStream openAppend()
		{
			try
			{
				Stream stream;
				if (!this.file.Exists)
					stream = this.file.Create();
				else
					stream = this.file.Open(FileMode.Append, FileAccess.Write);
				return new ScriptStream(this.System, stream);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine("Could not open file \"{0}\" for append access: {1}", this.file.Name, ex.Message);
				return null;
			}
		}

		#endregion
	}
}
